
# Private notebook files　:smile:
--------------------------------------------------
Welcome this is morishin web-page

# File List　:cry:   
--------------------------------------------------

* [000_index.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/000_index.ipynb)
* [001_Diary.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/001_Diary.ipynb)
* [002_README.md作成.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/002_README.md作成.ipynb)
* [miscellaneous/001_Begginner.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/miscellaneous/001_Begginner.ipynb)
* [miscellaneous/201401_SphinxJP2014.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/miscellaneous/201401_SphinxJP2014.ipynb)
* [slideshow/000_Test.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/slideshow/000_Test.ipynb)
* [study/00_Dockerからバージョンアップ方法.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/00_Dockerからバージョンアップ方法.ipynb)
* [study/20141026_OverShoot_Simulation.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20141026_OverShoot_Simulation.ipynb)
* [study/20150820_MachineLearning_scikit-learn.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150820_MachineLearning_scikit-learn.ipynb)
* [study/20150822_Pythonによるデータ分析入門.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150822_Pythonによるデータ分析入門.ipynb)
* [study/20150829_brokh.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150829_brokh.ipynb)
* [study/20150830_3D_PlotView.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150830_3D_PlotView.ipynb)
* [study/20150830_MP4.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150830_MP4.ipynb)
* [study/20150831_Create_Heatmap.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150831_Create_Heatmap.ipynb)
* [study/20150901_Kparaプロットサンプル.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150901_Kparaプロットサンプル.ipynb)
* [study/20150902_GPS座標データ読み出し.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150902_GPS座標データ読み出し.ipynb)
* [study/20150905_SlideshowTesting.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150905_SlideshowTesting.ipynb)
* [study/20150913_Jupyter_DownloadPdf.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20150913_Jupyter_DownloadPdf.ipynb)
* [study/20160205_notebook2でgccを使う方法.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160205_notebook2でgccを使う方法.ipynb)
* [study/20160211_Notebook_gccコンパイル方法.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160211_Notebook_gccコンパイル方法.ipynb)
* [study/20160417_seaborn_美しく描画.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160417_seaborn_美しく描画.ipynb)
* [study/20160612_Jupyterとpandasの表に色をつける.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160612_Jupyterとpandasの表に色をつける.ipynb)
* [study/20160613_Demo of plantUML in ipython notebook.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160613_Demo of plantUML in ipython notebook.ipynb)
* [study/20160613_UML Plantuml StartUp.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160613_UML Plantuml StartUp.ipynb)
* [study/20160709_JavaScriptTest.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160709_JavaScriptTest.ipynb)
* [study/20160709_OpticalSimulation_RayTrace.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160709_OpticalSimulation_RayTrace.ipynb)
* [study/20160709_OpticalSimulationZernike.ipynb](http://nbviewer.jupyter.org/urls/bitbucket.org/morishin8838/nbviewer/raw/master/study/20160709_OpticalSimulationZernike.ipynb)


--------------------------------------------------
Thanks!
:yum:

